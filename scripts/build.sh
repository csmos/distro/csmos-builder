#!/bin/sh
#
# SPDX-License-Identifier: GPL-2.0-only
#
# Copyright (C) 2022 csmos.org
#
# Author:
# Daniel Gomez <dagmcr@gmail.com>
#
# Start a Yocto Project/OpenEmbedded build.

wget https://raw.githubusercontent.com/siemens/kas/master/kas-container -O kas-container
chmod +x kas-container
DL_DIR=$PWD/downloads SSTATE_DIR=$PWD/sstate-cache ./kas-container \
--ssh-agent ~/.ssh build --force-checkout --update kas/csmos.yml
rm -rf kas-container

#!/bin/sh
#
# SPDX-License-Identifier: GPL-2.0-only
#
# Copyright (C) 2022 csmos.org
#
# Author:
# Daniel Gomez <dagmcr@gmail.com>
#
# Start bitbake shell using the kas-container.

scripts_dir=$(dirname "$0")
csmos_builder_dir=$(dirname "$scripts_dir")

wget https://raw.githubusercontent.com/siemens/kas/master/kas-container -O $PWD/kas-container
chmod +x $PWD/kas-container
$PWD/kas-container --version
KAS_CONTAINER_ENGINE=podman DL_DIR=$PWD/downloads SSTATE_DIR=$PWD/sstate-cache ./kas-container \
--ssh-agent shell --force-checkout --update kas/csmos.yml
rm -rf kas-container
